# Cabin

## Isolated (Internet-less) Movie Manager

### General Concept
Within a home network there is a homogeneous set of servers.  The servers can be divided into broad categories based on their abilities, though some may exist in multiple categories simultaneously due to their enhanced abilities.  The general set of abilities currently consists of servers that can:
1. Serve internally-hosted content over a network connection
2. Display content consumed from a server to a physically-attached display device

A content presenter (run in either a desktop or web environment) receives orders from a commanding message queue. Messages on this queue are inserted at the behest of a phone-acessible UI. Any request to display content results in a traversal of the network graph (through a database) to decide which content server is closest. A network request is then made to this content server for the requested media.  The results of this request are buffered and displayed on the attached display device.  

A content server consists of a daemon that simply returns a stream of bytes upon the request of a caller. At the initial stage of the project there is no need for the content server to perform any function beyond the role of a customized file server.

### Deployment Strategy
SHORT-TERM:
Just deploy (manually as a binary - no Docker) the presenter app onto the living room raspberry pi with an HDMI attached to the TV.  Deploy the content server, graph database, and MQTT broker (manually as a binary - no Docker) to the raspberry pi in my office that has RAID 1-arranged external hard drives with all my movies.  I am leaving this deployment strategy intentionally Spartan so that I don't get hung up on DevOps tinkering like I have with other projects.

LONG-TERM:
All applications will be managed by K3S, a minimal Kubernetes distribution.  There will only be one master node since I don't care about resiliency and mainly want to run Docker containers and use Kubernetes for its deployment features.  The master node will be on the local network, not in the cloud, to align with the goal of making this project internet-less.  I would like to store my code on two git servers, one in the cloud on a managed git server to protect against catastrophic failures (goodbye internet-less purity), and another locally run on an ARM processor that will likely just be a set of bash scripts.  To go all-in I will likely also utilize a local network-hosted docker registry (https://docs.docker.com/registry/deploying/).  

### Components
1. Content Presenter - The application responsible for displaying videos will be an Electron app.  The raspberry pi will be on perpetually, with this app running at full screen.  It will receive `display content` commands via a subscription to an MQTT message queue.  Each physical display will have a unique name, and this name will be the topic to which the content presenter will subscribe.  Reception of a `display content` command will result in the presenter querying the Neo4j database for the IP address of a content server hosting the required content with the fastest internet connection. The presenter will then send a request for the desired media to the determined server.   
2. Neo4j Database - The Neo4j database will not be part of the K3s deployment to improve simplicity.  It will serve to 
3. Commander - The commander will be a mobile-acessible (React) app, the only one that the user directly interacts with.  In the short term tt will be served from the office raspberry pi, but may eventually be served through any of the K8s worker nodes.
4. Content Server - The content server will simply be an instance of a file server, graciously provided by the go library.