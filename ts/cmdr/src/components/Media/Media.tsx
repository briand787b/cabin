import React, { FC, useEffect, useState } from "react";
import { VideoStore } from "../../lib/video";

type MediaProps = {
  videoID: string;
  videoStore: VideoStore;
};

const Media: FC<MediaProps> = ({ videoID, videoStore }) => {
  const [host, setHost] = useState<string | null>(null);
  useEffect(() => {
    async () => {
      setHost(await videoStore.getHost(videoID));
    };
  }, []);

  return (
    <div>
      <p>Video is available on {host} server</p>
    </div>
  );
};

export default Media;
