export class VidoeMockStore {
    dbHost: string

    constructor(dbHost: string) {
        this.dbHost = dbHost;
    }

    getHost(dbHost: string) {
        console.log(`generating fake data: | dbHost: ${dbHost}`)
        return new Promise<string>((resolve, reject) => {
            resolve(this.dbHost);
        });
    }
};