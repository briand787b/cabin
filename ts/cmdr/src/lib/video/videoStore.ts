export interface VideoStore {
    getHost(id: string): Promise<string>;
}